import { terser } from "rollup-plugin-terser";

module.exports = {
  input: 'src/hel2.js',
  output: {
    file: 'static/js/hel2.js',
    format: 'iife'
  },
  plugins: [ terser() ]
};
