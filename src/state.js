/**
 * This structure serves as a singleton state object,
 * shared between pages and views of the Hel2 app
 */
var state = {
  hint: true
};

export { state };
