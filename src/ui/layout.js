import { Navbar } from "./navbar.js";

var Layout = {
  view: function(vnode) {
    return [
      m(Navbar),
      m("div", { class: "content" }, vnode.children)
    ];
  }
};

export { Layout };
