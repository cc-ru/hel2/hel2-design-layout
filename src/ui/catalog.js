import { state } from "../state.js";

import { Card } from "./card.js";
import { Search } from "./search.js";

var Catalog = {
  view: function(vnode) {
    let elements = [
      m("div", { class: "title" }, "New Packages"),
      m("div", { class: "tile is-ancestor"}, [
        m("div", { class: "tile is-parent"}, [
          m(Card, { class: "tile is-child"}, [
            m("div", { class: "title" }, "holo"),
            m("div", "Hologram editor for OpenComputers. Allows to create, render and share holographic models."),
            m("br"),
            m("span", { class: "tag" }, "lua"),
            m("span", { class: "tag" }, "hologram"),
            m("span", { class: "tag" }, "oc"),
          ]),
          m(Card, { class: "tile is-child"}, [
            m("div", { class: "title" }, "holo"),
            m("div", "Hologram editor for OpenComputers. Allows to create, render and share holographic models."),
            m("br"),
            m("span", { class: "tag" }, "lua"),
            m("span", { class: "tag" }, "hologram"),
            m("span", { class: "tag" }, "oc"),
          ]),
          m(Card, { class: "tile is-child"}, [
            m("div", { class: "title" }, "holo"),
            m("div", "Hologram editor for OpenComputers. Allows to create, render and share holographic models."),
            m("br"),
            m("span", { class: "tag" }, "lua"),
            m("span", { class: "tag" }, "hologram"),
            m("span", { class: "tag" }, "oc"),
          ]),
          m(Card, { class: "tile is-child"}, [
            m("div", { class: "title" }, "holo"),
            m("div", "Hologram editor for OpenComputers. Allows to create, render and share holographic models."),
            m("br"),
            m("span", { class: "tag" }, "lua"),
            m("span", { class: "tag" }, "hologram"),
            m("span", { class: "tag" }, "oc"),
          ]),
        ]),
      ]),
    ];
    if (state.hint) {
      elements.unshift(m(Card, { class: "hint" }, [
        m("div", { class: "title", style: "margin-top: 0;" }, "Hel2 Repository"),
        m("div", "Repository for your OpenComputers stuff."),
        m("div", "Pack your sources, move to Hel!"),
        m("div", { class: "title" }, "How to start?"),
        m("div", "1. Install HPM:"),
        m("div", { class: "console" }, "pastebin run XXXXXXX"),
        m("div", "2. Install whatever you want:"),
        m("div", { class: "console" }, "hpm install whatever"),
        m("div", "3. Profit!"),
      ]));
    }
    return elements;
  }
};

export { Catalog };
