var Card = {
  view: function(vnode) {
    console.log(vnode);
    return m("div", { class: "card-wrap " + vnode.attrs.class },
      m("div", { class: "card" }, vnode.children)
    );
  }
};

export { Card };
