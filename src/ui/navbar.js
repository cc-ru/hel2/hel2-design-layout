var Navbar = {
  view: function(vnode) {
    return m("div", { class: "navbar noselect" }, [
      m("div", "All"),
      m("div", { class: "selected" }, "New"),
      m("div", "Popular"),
      m(m.route.Link, { class: "logo", href: "/", ondragstart: function() { return false; } },
        m("img", { src: "images/logo.png" })
      ),
      m("div", "Help"),
      m("div", "Profile"),
      m("div", "Logout"),
    ]);
  }
};

export { Navbar };
