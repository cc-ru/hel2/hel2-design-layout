import { Layout } from "./ui/layout.js";
import { Catalog } from "./ui/catalog.js";

m.route(document.body, "/", {
  "/": {
    render: function() {
      return m(Layout, m(Catalog))
    }
  }
});
